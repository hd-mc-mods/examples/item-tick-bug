# Example

This example illustrates the [issue](https://forums.minecraftforge.net/topic/141910-item-dupe-when-tossed-while-tag-is-beeing-updated-in-inventorytick-ghost-item/) with changes to ticking items may causing duplicates when beeing dropped. The issue seems only to arise during lag spikes.

## Fixes

A workaround to prevent this bug from happening was found and implemented on the branch [fix/on_dropped_by_player](https://gitlab.com/hd-mc-mods/examples/item-tick-bug/-/tree/fix/on_dropped_by_player?ref_type=heads).

## How to reproduce

- Put one of the example items into the hotbar (found in the creative tab 'Tools')
- Drop the item into the world while the game is lagging (try a few times)

![item-tag-bug.gif](item-tag-bug.gif)
