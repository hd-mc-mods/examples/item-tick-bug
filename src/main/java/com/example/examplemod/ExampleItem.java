package com.example.examplemod;

import java.util.List;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ExampleItem extends Item {
    static class Properties extends Item.Properties {
        public Properties() {
            tab(CreativeModeTab.TAB_TOOLS);
            stacksTo(1);
        }
    }

    public ExampleItem() {
        super(new Properties());
    }

    @Override
    public void inventoryTick(ItemStack stack, Level level, Entity entity, int i, boolean bl) {
        if(!level.isClientSide) {
            stack.getOrCreateTag().putInt("test", (int)(1000*Math.random()));
        }
    }

    @Override
    public void appendHoverText(ItemStack stack, Level level, List<Component> components, TooltipFlag flag) {
        components.add(Component.literal(String.valueOf(stack.getOrCreateTag().getInt("test"))));
    }
}
